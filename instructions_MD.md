# TEXT
Ways to display a Normal, Bold, Italic, Strikethrough text.

Simple text :- Can be simply typed → Text
Italic text :- Can be typed between pair of Underscores → _text_
Bold text :- Can be typed between a pair of two Asterisk → **text**
Strikethrough :- Can be typed between a pair of two Tilde → ~~text~~

# HEADING
Heading can be typed in different sizes ranging from 1–6 by adding hash/pound. It can be typed in the following ways.

# Heading 1
## Heading 2
### Heading 3
#### Heading 4
##### Heading 5
###### Heading 6

# QUOTE and COPYRIGHT
To add Quote simply use strict inequality GREATER THAN symbol.

> Your Quote will be typed here

For COPYRIGHT sign follow this method.

&copy;

# HYPERLINK
To add a hyperlink in your .md file follow this rule.

Type the name of hyperlink inside square brackets i.e. [ ]
And type/paste the link or URL inside round brackets i.e. ( )
Example

[YouTube](https://www.youtube.com "YouTube")

# IMAGE FILE
To add an image file in your .md file follow the following rules.

Type the image name inside square brackets after adding an Exclamation point/BANG → ! [ ].
And type/paste the image link or URL inside round brackets i.e. ( )
Example

![Dog](https://picsum.photos/id/237/200/300)

# CODE
To add a code in .md file type the code in between a pair of BACKQUOTE (`code`).

`print('Hello, world!')`

# CODE BLOCKS
To add a Block of multiple lines of codes in a .md file, type it between a pair of 3 BLOCKQUOTE (```code blocks```).

```py
print(2)
print("Dog")
```

# TABLE
To create a table in a .md file follow the instructions using the image(Table for car model) as reference.

* In the line-3 heading of the table is added separated by a pipe ( | )
* In the line-4 size is provided for the content of table using dash (-), no of dashes represent the size of that particular column, any character/word typed cannot exceed the size provided using dashes.
* In the line-5,6 contents of the table are added.

|Sr.no | Name | NO. |
|--    |------|-----|
|01    |Skoda |0CB56|
|02    |Ford  |0CC56|

> NOTE: Content of table (character/word/no.) should not exceed the size provided, if it is exceeding increase the size by adding more dashes.

# LIST
To create a List(ordered/unordered) follow the codes are given shown below.

1. Item
2. Item
    * Sub item 1
    * Sub item 2
3. Item
* Unordered item 1
* Unordered item 2
* Unordered item 3

# TASK LIST
To make a task list in a .md file, add a dash(-) followed by square brackets [ ] further followed by task.

- [x]   taks 1
- [ ]   task 2
- [ ]   task 3

# LINES
There are many ways to create a line inside a .md file some of them are shown below.

To make a simple line use 3 dashes or asterisk.

> (- - -) or (***)

line 1
***
line 2
---
---
line 3
=
===============================
line 4
***
***
line 5
===
---

# ESCAPE CHARACTER
To display a literal character that would otherwise be used to format text in a Markdown document, add a backslash (\) in front of the character.

\* Without the backslash, this would be a bullet in an unordered list.

The rendered output looks like this:

* Without the backslash, this would be a bullet in an unordered list.
