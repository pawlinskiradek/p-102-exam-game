class Item:
    def __init__(self, name, description):
        self.name = name
        self.description = description

class Room:
    def __init__(self, description, items=None):
        self.description = description
        self.items = items if items is not None else []

    def add_item(self, item):
        self.items.append(item)

    def remove_item(self, item):
        self.items.remove(item)

class Game:
    def __init__(self):
        # Creating rooms and their initial items
        self.rooms = {
            'living_room': Room("You are in the living room. There is a key on the table.", [Item("key", "an old, iron key")]),
            'kitchen': Room("You are in the kitchen. You see a bottle with a potion on the shelf.", [Item("potion", "a green potion")])
        }
        # Setting the player's initial room to the living room
        self.current_room = self.rooms['living_room']
        # Initializing the player's items list
        self.items = []

    def run(self):
        # Displaying the welcome message and available commands
        print("Welcome to the text adventure game!")
        print("To quit, type 'quit'.")
        print("Available commands:")
        print("describe - describes the current room")
        print("items - displays the list of items")
        print("take [item_name] - takes an item")
        print("use [item_name] - uses an item")
        print("go [room_name] - moves to another room")

        # Game loop
        while True:
            # Displaying the description of the current room
            self.describe_room()
            # Reading the player's action
            action = input("What do you want to do? ").lower().split()

            # Handling different player actions
            if action[0] == 'quit':
                print("Thank you for playing!")
                break
            elif action[0] == 'describe':
                self.describe_room()
            elif action[0] == 'items':
                self.display_items()
            elif action[0] == 'take':
                if len(action) < 2:
                    print("Please provide the name of the item you want to take.")
                else:
                    self.take_item(action[1])
            elif action[0] == 'use':
                if len(action) < 2:
                    print("Please provide the name of the item you want to use.")
                else:
                    self.use_item(action[1])
            elif action[0] == 'go':
                if len(action) < 2:
                    print("Please provide the name of the room you want to go to.")
                else:
                    self.go_to_room(action[1])
            else:
                print("Unknown command. Type 'help' to see the list of available commands.")

    def describe_room(self):
        # Displaying the description of the current room
        print(self.current_room.description)
        # Displaying the items in the room
        if self.current_room.items:
            print("You see the following items:")
            for item in self.current_room.items:
                print("-", item.name)

    def display_items(self):
        # Displaying the list of player's items
        if self.items:
            print("You have the following items:")
            for item in self.items:
                print("-", item.name)
        else:
            print("You have no items.")

    def take_item(self, item_name):
        # Taking an item from the room
        for item in self.current_room.items:
            if item.name.lower() == item_name:
                self.current_room.remove_item(item)
                self.items.append(item)
                print(f"You took the {item.name}.")
                return
        print("There is no such item here.")

    def use_item(self, item_name):
        # Using an item from the player's items list
        for item in self.items:
            if item.name.lower() == item_name:
                # Special logic for using the key to open the door
                if self.current_room == self.rooms['living_room'] and item.name.lower() == 'key':
                    print("You opened the door to the kitchen!")
                    self.current_room = self.rooms['kitchen']
                else:
                    print("There is nothing to use this item on here.")
                return
        print("You don't have such item or cannot use it here.")

    def go_to_room(self, room_name):
        # Moving to another room
        if room_name in self.rooms:
            self.current_room = self.rooms[room_name]
            print(f"Now you are in the {room_name}.")
        else:
            print("There is no such room.")


# Initializing the game and running it
game = Game()
game.run()
