class Przedmiot:
    def __init__(self, nazwa, opis):
        self.nazwa = nazwa
        self.opis = opis

class Pomieszczenie:
    def __init__(self, opis, przedmioty=None):
        self.opis = opis
        self.przedmioty = przedmioty if przedmioty is not None else []

    def dodaj_przedmiot(self, przedmiot):
        self.przedmioty.append(przedmiot)

    def usun_przedmiot(self, przedmiot):
        self.przedmioty.remove(przedmiot)

class Gra:
    def __init__(self):
        # Tworzenie pomieszczeń oraz ich początkowych przedmiotów
        self.pomieszczenia = {
            'salon': Pomieszczenie("Jesteś w salonie. Na stole leży klucz.", [Przedmiot("klucz", "stary, żelazny klucz")]),
            'kuchnia': Pomieszczenie("Jesteś w kuchni. Na półce widzisz butelkę z miksturą.", [Przedmiot("mikstura", "zielona mikstura")])
        }
        # Ustawienie początkowego pomieszczenia gracza na salon
        self.obecne_pomieszczenie = self.pomieszczenia['salon']
        # Inicjacja listy przedmiotów gracza
        self.przedmioty = []

    def uruchom(self):
        # Wyświetlenie informacji powitalnej oraz dostępnych komend
        print("Witaj w grze tekstowej!")
        print("Aby zakończyć, wpisz 'koniec'.")
        print("Dostępne komendy:")
        print("opisz - opisuje aktualne pomieszczenie")
        print("przedmioty - wyświetla listę przedmiotów")
        print("zabierz [nazwa_przedmiotu] - zabiera przedmiot")
        print("użyj [nazwa_przedmiotu] - używa przedmiotu")
        print("idź [nazwa_pomieszczenia] - przechodzi do innego pomieszczenia")

        # Pętla gry
        while True:
            # Wyświetlenie opisu aktualnego pomieszczenia
            self.opisz_pomieszczenie()
            # Wczytanie akcji gracza
            akcja = input("Co chcesz zrobić? ").lower().split()

            # Obsługa różnych akcji gracza
            if akcja[0] == 'koniec':
                print("Dziękujemy za grę!")
                break
            elif akcja[0] == 'opisz':
                self.opisz_pomieszczenie()
            elif akcja[0] == 'przedmioty':
                self.wyswietl_przedmioty()
            elif akcja[0] == 'zabierz':
                if len(akcja) < 2:
                    print("Podaj nazwę przedmiotu, który chcesz zabrać.")
                else:
                    self.zabierz_przedmiot(akcja[1])
            elif akcja[0] == 'użyj':
                if len(akcja) < 2:
                    print("Podaj nazwę przedmiotu, który chcesz użyć.")
                else:
                    self.uzyj_przedmiot(akcja[1])
            elif akcja[0] == 'idź':
                if len(akcja) < 2:
                    print("Podaj nazwę pomieszczenia, do którego chcesz przejść.")
                else:
                    self.idz_do_pomieszczenia(akcja[1])
            else:
                print("Nieznana komenda. Wpisz 'pomoc' aby zobaczyć listę dostępnych komend.")

    def opisz_pomieszczenie(self):
        # Wyświetlenie opisu aktualnego pomieszczenia
        print(self.obecne_pomieszczenie.opis)
        # Wyświetlenie przedmiotów w pomieszczeniu
        if self.obecne_pomieszczenie.przedmioty:
            print("Widzisz następujące przedmioty:")
            for przedmiot in self.obecne_pomieszczenie.przedmioty:
                print("-", przedmiot.nazwa)

    def wyswietl_przedmioty(self):
        # Wyświetlenie listy przedmiotów gracza
        if self.przedmioty:
            print("Posiadasz następujące przedmioty:")
            for przedmiot in self.przedmioty:
                print("-", przedmiot.nazwa)
        else:
            print("Nie masz żadnych przedmiotów.")

    def zabierz_przedmiot(self, nazwa_przedmiotu):
        # Zabranie przedmiotu z pomieszczenia
        for przedmiot in self.obecne_pomieszczenie.przedmioty:
            if przedmiot.nazwa.lower() == nazwa_przedmiotu:
                self.obecne_pomieszczenie.usun_przedmiot(przedmiot)
                self.przedmioty.append(przedmiot)
                print(f"Zabrałeś {przedmiot.nazwa}.")
                return
        print("Nie ma takiego przedmiotu.")

    def uzyj_przedmiot(self, nazwa_przedmiotu):
        # Użycie przedmiotu z listy przedmiotów gracza
        for przedmiot in self.przedmioty:
            if przedmiot.nazwa.lower() == nazwa_przedmiotu:
                # Specjalna logika dla użycia klucza do otwarcia drzwi
                if self.obecne_pomieszczenie == self.pomieszczenia['salon'] and przedmiot.nazwa.lower() == 'klucz':
                    print("Otworzyłeś drzwi do kuchni!")
                    self.obecne_pomieszczenie = self.pomieszczenia['kuchnia']
                else:
                    print("Nie ma tu nic do użycia tego przedmiotu.")
                return
        print("Nie masz takiego przedmiotu lub nie możesz go użyć tutaj.")

    def idz_do_pomieszczenia(self, nazwa_pomieszczenia):
        # Przejście do innego pomieszczenia
        if nazwa_pomieszczenia in self.pomieszczenia:
            self.obecne_pomieszczenie = self.pomieszczenia[nazwa_pomieszczenia]
            print(f"Jesteś teraz w {nazwa_pomieszczenia}.")
        else:
            print("Nie ma takiego pomieszczenia.")


# Inicjacja gry i uruchomienie
gra = Gra()
gra.uruchom()