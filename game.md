# Tytuł gry: "Ucieczka z więzienia"

## Więzień
> **worek** - umieszczamy w nim przedmioty (pojemność = 2)
_użyj_ - używasz przedmiotu
_schowaj_ - chowasz przedmiot do torby
_wyrzuć_ - wyrzucasz przedmiot z torby
_co-widzę_ - opisuje ścianę
_obróć-się-w-lewo_ - obrót w lewo
_obróć-się-w-prawo_ - obrót w prawo

## Cela (pomieszczenie nr 1)
#### ściana PN
1. Okno z kratą (widok na morze)

#### ściana PD
1. Drzwi zamknięte (zamek w drzwiach)

#### ściana ZCHD
1. Łóżko metalowe
    * materac słomiany
        * wystająca sprężyna (można użyć jako wytrych - przedmiot)
    * poduszka (przedmiot)
    * koc (przedmiot)
2. Stolik drewniany
    * kubek (przedmiot)
3. Krzesło drewniane

#### ściana WSCH
1. zlew
2. kibel

## Korytarz (pomieszczenie nr 2)
#### ściana PN
1. Drzwi otwarte do celi

#### ściana PD
1. Drzwi wyjściowe zamknięte (zamek na 4 cyfrowy kod)

#### ściana ZCHD
1. Graffiti z głową smoka

#### ściana WSCH
1. Półka z książkami
    * tytuł "W**y**jście Smoka" (przedmiot)
        > przepis:
            - [ ] 3 cytryny
            - [ ] 0 soli
            - [ ] 5 jaj
            - [ ] 2 kostki masła
    * tytuł "Cela" (przedmiot)
        > Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
    * tytuł "Jak ukraśc pierwszy milion" (przedmiot)
        > Libero enim sed faucibus turpis in eu. Volutpat diam ut venenatis tellus in metus vulputate eu. Eu mi bibendum neque egestas congue quisque egestas diam in. Etiam erat velit scelerisque in dictum non consectetur. At consectetur lorem donec massa sapien faucibus et molestie ac. Turpis in eu mi bibendum neque egestas congue. Eu mi bibendum neque egestas congue quisque egestas. Ante in nibh mauris cursus mattis molestie a iaculis. Et malesuada fames ac turpis. Nulla pharetra diam sit amet.